<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.2.2-Bonn" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+8">
  <pipe>
    <rasterrenderer classificationMax="90" type="singlebandpseudocolor" opacity="1" classificationMin="0" alphaBand="-1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" classificationMode="2" clip="0">
          <colorramp type="gradient" name="[source]">
            <prop v="26,150,65,255" k="color1"/>
            <prop v="215,25,28,255" k="color2"/>
            <prop v="0" k="discrete"/>
            <prop v="gradient" k="rampType"/>
            <prop v="0.25;166,217,106,255:0.5;255,255,192,255:0.75;253,174,97,255" k="stops"/>
          </colorramp>
          <item color="#1a9641" alpha="255" value="0" label="0"/>
          <item color="#60b855" alpha="255" value="5" label="5.96"/>
          <item color="#a6d96a" alpha="255" value="10" label="11.9"/>
          <item color="#d3ec95" alpha="255" value="15" label="17.9"/>
          <item color="#ffffc0" alpha="255" value="20" label="23.9"/>
          <item color="#fed791" alpha="255" value="25" label="29.8"/>
          <item color="#fdae61" alpha="255" value="30" label="35.8"/>
          <item color="#ea633e" alpha="255" value="35" label="41.7"/>
          <item color="#d7191c" alpha="255" value="90" label="47.7"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeRed="255" saturation="0" colorizeOn="0" grayscaleMode="0" colorizeBlue="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
