@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script for create tilled slope map LAS with cutting to SHP (if exists)
ECHO Slope Map from Terrain model: NMT
ECHO Author: Marek Szczepkowski
ECHO Date: 25.07.2019
ECHO Version: 1.0
ECHO For SAGA 6.0, LASTool, QGIS 3.0+ (or earlier)


SET SAGA_ROOT=e:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\
SET QGIS_ROOT=C:\Program Files\QGIS 3.8
SET LASTOOLS_ROOT=e:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\release_1.0.0\LAStools\
SET COORDINATE=3006


REM Setup saga_cmd.exe
SET PATH=%PATH%;%SAGA_ROOT%
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
REM Setup txt2las.exe
SET xyz2las=%LASTOOLS_ROOT%\bin\txt2las.exe

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_output_nmt
SET OUT_SUFFIX_SLOPE=_output_nmt_slope

REM COUNTER FILES
dir /b *.las 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
    ECHO Processing  %%i     !Counter! / %count% FILES
    saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES:"%WORK%\%%i" -POINTS:"%WORK%\%%i" -RGB_RANGE:0
    saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS="%WORK%\%%~ni.sg-pts" -GRID="%WORK%\%%~ni.sgrd" -OUTPUT:0 -AGGREGATION:3 -CELLSIZE:0.500000
    saga_cmd --flags=r grid_tools "Close Gaps" -INPUT:"%WORK%\%%~ni.sgrd" -RESULT:"%WORK%\%%~ni_closed_gaps.sgrd" -THRESHOLD:0.100000
    saga_cmd --flags=r grid_filter "Gaussian Filter" -INPUT:"%WORK%\%%~ni_closed_gaps.sgrd" -RESULT:"%WORK%\%%~ni_output.sgrd" -SIGMA:1.000000 -KERNEL_TYPE:1 -KERNEL_RADIUS:5
    
    REM Output ASC
    saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"%WORK%\%%~ni_output.sgrd" -FILE:"%WORK%\%%~ni%OUT_SUFFIX%.asc" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0
	
	REM Slope Map
	gdaldem slope -compute_edges "%WORK%\%%~ni%OUT_SUFFIX%.asc" "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%.asc"
    gdaldem color-relief -of GTiff -co "TFW=YES" "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%.asc" "slope_color_legend.txt" "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%_color.tif" 	
    IF EXIST "%WORK%\*.shp" gdalwarp -s_srs EPSG:%COORDINATE% -co "TFW=YES" -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0 " -cutline *.shp "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%_color.tif" "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%_color_cut.tif" & DEL /Q "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%_color.tif" & DEL /Q "%WORK%\%%~ni%OUT_SUFFIX_SLOPE%_color.tfw"
    ECHO.
    
    DEL /Q "%WORK%\*.mgrd"
    DEL /Q "%WORK%\*.prj"
    DEL /Q "%WORK%\*.sdat" 
    DEL /Q "%WORK%\*.sgrd"
    DEL /Q "%WORK%\*.sg-pts"
    DEL /Q "%WORK%\*.sg-info"
	set /A Counter+=1	
)
ECHO.
set /P c=Are you want to merge TIFs[Y/N]?
if /I "%c%" EQU "Y" goto :merge
if /I "%c%" EQU "N" goto :end
:merge
ECHO.
ECHO Merging tiffs
gdalbuildvrt "%WORK%\Merge.vrt" *.tif 
gdalwarp -s_srs EPSG:%COORDINATE% -co "TFW=YES" -ot Byte -of GTiff -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0" "%WORK%\Merge.vrt" "%WORK%\Merge.tif"
DEL /Q "%WORK%\*.vrt" 
:end
PAUSE