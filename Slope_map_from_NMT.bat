@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script for create tilled slope map from NMT(ASC FILE) with cutting to SHP (if exists)
ECHO Slope Map from Terrain model: NMT
ECHO Author:Marek Szczepkowski
ECHO Date: 25.07.2019
ECHO Version: 1.0
ECHO For SAGA 6.0, QGIS 3.0+ (or earlier)


SET SAGA_ROOT=e:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\
SET QGIS_ROOT=C:\Program Files\QGIS 3.8
SET COORDINATE=3006

REM Setup saga_cmd.exe
SET PATH=%PATH%;%SAGA_ROOT%
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_slope
REM COUNTER FILES
dir /b *.asc 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.asc"') DO (
    ECHO Processing  %%i    !Counter! / %count% FILES
    gdaldem slope -compute_edges %%i "%WORK%\%%~ni%OUT_SUFFIX%.asc"
    gdaldem color-relief -of GTiff -co "TFW=YES" "%WORK%\%%~ni%OUT_SUFFIX%.asc" "slope_color_legend.txt" "%WORK%\%%~ni%OUT_SUFFIX%_color.tif" 	
    IF EXIST "%WORK%\*.shp" gdalwarp -co "TFW=YES" -of GTiff -s_srs EPSG:%COORDINATE% -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0 " -cutline *.shp "%WORK%\%%~ni%OUT_SUFFIX%_color.tif" "%WORK%\%%~ni%OUT_SUFFIX%_color_cut.tif" 
	& DEL /Q "%WORK%\%%~ni%OUT_SUFFIX%_color.tif" & DEL /Q "%WORK%\%%~ni%OUT_SUFFIX%_color.tfw"
    ECHO.
	set /A Counter+=1	
)
ECHO.
set /P c=Are you want to merge TIFs[Y/N]?
if /I "%c%" EQU "Y" goto :merge
if /I "%c%" EQU "N" goto :end
:merge
ECHO.
ECHO Merging tiffs
gdalbuildvrt "%WORK%\Merge.vrt" *.tif 
gdalwarp -s_srs EPSG:%COORDINATE% -co "TFW=YES" -ot Byte -of GTiff -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0" "%WORK%\Merge.vrt" "%WORK%\Merge.tif"
DEL /Q "%WORK%\*.vrt" 
:end
PAUSE